import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'learning';
  
}
function addNumbers(a: number, b: number) {  
  return a + b;  
}  
var sum = addNumbers(15, 25);  
console.log('Sum of the numbers is: ' + sum);
// 
function greeter(person: string) {  
  return "Hello, " + person;  
}  
let user = 'hello world';  
console.log(greeter(user));
  // 
  function ProcessData(x: any, y: any) {  
    return x + y; 
}  
let result: any;
console.log(result)  
result = ProcessData("Hello ", "Any!"); //Hello Any!  
result = ProcessData(2, 3); //5 
// 
function get(x: number){     
  console.log(x);        
  var a = x;        
  console.log(a);    
}    
get(4);  
// 
// function constTest(){  
//   const VAR = 10;  
//   console.log("Output: " +VAR);  // Output: 10  
//   const VAR = 10;  
//   console.log("Output: " +VAR);  //Uncaught TypeError: Assignment to constant variable  
// }  
// constTest(); 
// 
var greter = "hey hi";  
var times = 5;  
if (times > 3) {  
   var greter = "heo world";   
}  
console.log(greter)
// 
let greater = "hey hi";  
let time = 5;  
if (times > 3) {  
   let hello = "heo";   
   console.log(hello) 
}  
console.log(hello)


function hello(hello: any) {
  throw new Error('Function not implemented.');
}
// 
function display(id:number, name:string)  
{  
    console.log("Id = " + id + ", Name = " + name);  
}  
display(100, "kapardee is the best ");  
// 
function add(x: number, y: number )  
{  
    return x + y;      
}  
let Addition: number = add(10,20);
// 
interface Student {   
  name: string;   
  code: number;   
}  
let student = <Student> { };   
student.name = "malinga"; // Correct  
student.code = 123;
console.log(student)
// 
let arr:number[];   
arr = [1, 2, 3, 4]   
console.log("Array[0]: " +arr[0]);   
console.log("Array[1]: " +arr[1]);  
// 
function displayed(value: (number | string))  
{  
    if(typeof(value) === "number")  
        console.log('The given value is of type number.');  
    else if(typeof(value) === "string")  
        console.log('The given value is of type string.');  
}  
displayed(123);  
displayed("ABC");  
// 
let arrType:number[]|string[];   
let i:number;   
arrType = [1,2,3,4];  
console.log("Numeric type array:")    
  
for(i = 0;i<arrType.length;i++){  
   console.log(arrType[i]);  
}  
arrType = ["India","America","England"];  
console.log("String type array:")    
  
for(i = 0;i<arrType.length;i++){   
   console.log(arrType[i]);  
}  
// 
type Pet = 'cat' | 'dog' | 'Rabbit';  
let pet: Pet;  
if(pet = 'cat'){  
    console.log("Correct");  
};  
// 
type FruitsName = "Apple" | "Mango" | "Orange";  
function showFruitName(fruitsName: FruitsName): void {  
    console.log(fruitsName);  
}  
showFruitName('Mango'); 
// 
let myNumber: number = 12345;  
let myNumber_1: number = 12.8789;  
let myNumber_2: number = 12667.976;  
let myNumber_3: number = 12.5779;  
let myNumber_4: number = 1234;  
let myNumber_5 = new Number(123);  
  
console.log("Number Method: toExponential()");  
console.log(myNumber.toExponential());   
console.log(myNumber.toExponential(2));  
  
console.log("Number Method: toString()");  
console.log(myNumber.toString());  
console.log(myNumber.toString(4));  
  
console.log("Number Method: toFixed()");  
console.log(myNumber_1.toFixed());  
console.log(myNumber_1.toFixed(3));  
  
console.log("Number Method: toLocaleString()");  
console.log(myNumber_2.toLocaleString()); // returns in US English  
  
console.log("Number Method: toPrecision()");  
console.log(myNumber_3.toPrecision(1));   
console.log(myNumber_3.toPrecision(3));  
  
console.log("Number Method: tovalueOf()");  
console.log(myNumber_5)  
console.log(myNumber_5.valueOf())  
console.log(typeof myNumber)  
// 
let n = 10  
if (n > 0) {   
   console.log("The input value is positive Number: " +n);  
} else {  
    console.log("The input value is negative Number: " +n);  
}  
// 
let marks = 95;  
if(marks<50){  
    console.log("fail");  
}  
else if(marks>=50 && marks<60){  
    console.log("D grade");  
}  
else if(marks>=60 && marks<70){  
    console.log("C grade");  
}  
else if(marks>=70 && marks<80){  
    console.log("B grade");  
}  
else if(marks>=80 && marks<90){  
    console.log("A grade");  
}else if(marks>=90 && marks<100){  
    console.log("A+ grade");  
}else{  
    console.log("Invalid!");  
}  
// 
let grade: string = "A";  
switch (grade)  
{   
    case'A+':  
      console.log("Marks >= 90"+"\n"+"Excellent");  
      break;  
  
    case'A':  
      console.log("Marks [ >= 80 and <90 ]"+"\n"+"Good");  
      break;  
  
    case'B+':  
      console.log("Marks [ >= 70 and <80 ]"+"\n"+"Above Average");  
      break;  
  
    case'B':  
      console.log("Marks [ >= 60 and <70 ]"+"\n"+"Average");  
      break;  
  
    case'C':  
      console.log("Marks < 60"+"\n"+"Below Average");  
      break;  
  
    default:  
        console.log("Invalid Grade.");  
}  
// 
let array = [1, 2, 3, 4, 5];  
  
for (var val of array) {  
  console.log(val);  
}  
// 
let myArray = [10, 20, 30, 40, 50,];  
console.log("Output of for..in loop ");  
for (let index in myArray) {  
   console.log(index);  
}  
console.log("Output of for..of loop ");  
for (let val of myArray) {  
   console.log(val);  
}  
// 
enum Direction {  
  Up = 1,  
  Down,  
  Left,  
  Right,  
}  
console.log(Direction);  
// 
let apps = ['WhatsApp', 'Instagram', 'Facebook'];  
let playStore: string[] = [];  
  
apps.forEach(function(item){  
  playStore.push(item)  
});  
  
console.log(playStore);
// 
let map = new Map();  
  
map.set('1', 'abhishek');     
map.set(1, 'www.javatpoint.com');       
map.set(true, 'bool1');   
map.set('2', 'ajay');  
  
console.log( "Value1= " +map.get(1)   );   
console.log("Value2= " + map.get('1') );   
console.log( "Key is Present= " +map.has(3) );   
console.log( "Size= " +map.size );   
console.log( "Delete value= " +map.delete(1) );   
console.log( "New Size= " +map.size );  
// 
let diceEntries = new Set();  
  
diceEntries.add(1).add(2).add(3).add(4).add(5).add(6);  
   
console.log("Dice Entries are:");   
for (let diceNumber of diceEntries) {  
    console.log(diceNumber);   
}  
   
console.log("Dice Entries with forEach are:");   
diceEntries.forEach(function(value) {  
  console.log(value);     
});  
// 
class Student {  
  public studCode!: number;  
  studName!: string;  
}  

let stud = new Student();  
stud.studCode = 101;  
stud.studName = "Joe Root";  

console.log(stud.studCode+ " "+stud.studName);  
// 
function summed(a: number, ...b: number[]): number {  
  let result = a;  
  for (var i = 0; i < b.length; i++) {  
  result += b[i];  
  }  
  return result;  
}  
let result1 = summed(3, 5);  
let result2 = summed(3, 5, 7, 9);  
console.log(result1 +"\n" + result2);  
// 
let myAdd = function (x: number, y: number) : number {  
  return x + y;  
};  
console.log(myAdd(5,3));  
// 
let adding = (a: number, b: number): number => {  
  return a + b;  
}  
console.log(adding(20, 30));  
// 
let myArrayed = [10, 20, 30, 40, 50,];  
console.log("Output of for..in loop ");  
for (let index in myArrayed) {  
   console.log(index);  
}  
console.log("Output of for..of loop ");  
for (let val of myArrayed) {  
   console.log(val);  
 } 
//  
var num = [5, 10, 15];  
num.forEach(function (value) {  
  console.log(value);  
});  
// 
let mapp = new Map();  
  
mapp.set('1', 'kapardee');     
mapp.set(1, 'payer');       
mapp.set(true, 'bool1');   
mapp.set('2', 'kappi');  
  
console.log( "Value1= " +mapp.get(1)   );   
console.log("Value2= " + mapp.get('1') );   
console.log( "Key is Present= " +mapp.has(3) );   
console.log( "Size= " +mapp.size );   
console.log( "Delete value= " +mapp.delete(1) );   
console.log( "New Size= " +mapp.size );  
// 
let ageMapping = new Map();  
   
ageMapping.set("Ramesh", 40);  
ageMapping.set("suresh", 25);  
ageMapping.set("kamesh", 30);  
   
for (let key of ageMapping.keys()) {  
    console.log("Map Keys= " +key);          
}  
for (let value of ageMapping.values()) {  
    console.log("Map Values= " +value);      
}  
console.log("The Map Enteries are: ");   
for (let entry of ageMapping.entries()) {  
    console.log(entry[0], entry[1]);   
}
//
let studentEntries = new Set();  
   
studentEntries.add("John");  
studentEntries.add("Peter");  
studentEntries.add("Gayle");  
studentEntries.add("Kohli");   
studentEntries.add("Dhawan");   
  
console.log(studentEntries);   
   
console.log(studentEntries.has("Kohli"));        
console.log(studentEntries.has(10));        
   
console.log(studentEntries.size);    
   
console.log(studentEntries.delete("Dhawan"));      
   
studentEntries.clear();   
  
console.log(studentEntries);  
// 
let studentEntry = new Set();  
   
studentEntry.add("John").add("Peter").add("Gayle").add("Kohli");  
  
console.log("The List of Set values:");  
console.log(studentEntry);  
// 
let diceEntry = new Set();  
  
diceEntry.add(1).add(2).add(3).add(4).add(5).add(6);  
   
console.log("Dice Entries are:");   
for (let diceNumber of diceEntry) {  
    console.log(diceNumber);   
}  
   
console.log("Dice Entries with forEach are:");   
diceEntry.forEach(function(value) {  
  console.log(value);     
});
 //
 function displaying() {  
  console.log("Hello ppp");  
}  
displaying();  
// 
let myAdding = function (x: number, y: number) : number {  
  return x + y;  
};  
console.log(myAdding(5,3));
 //
 function summ(a: number, ...b: number[]): number {  
  let result = a;  
  for (var i = 0; i < b.length; i++) {  
  result += b[i];  
  }  
  return result;  
}  
let result11 = summ(3, 5);  
let result22 = summ(3, 5, 7, 9);  
console.log(result11 +"\n" + result22);   
// 
interface OS {  
  name: String;  
  language: String;  
}  
let OperatingSystem = (type: OS): void => {  
console.log('Android ' + type.name + ' has ' + type.language + ' language.');  
};  
let Oreo = {name: 'O', language: 'Java'}  
OperatingSystem(Oreo);  
// 
let OperatingSystemed = (type: { name: any; language: any; }) => {  
  console.log('Android ' + type.name + ' has ' + type.language + ' language.');  
};  
let Oreod = { name: 'O', language: 'Java' };  
OperatingSystemed(Oreod);  
// 
interface nameArray {  
  [index:number]:string  
}  
let myNames: nameArray;  
myNames = ['Virat', 'Rohit', 'Sachin'];  

interface ageArray {  
  [index:number]:number  
}  
var myAges: ageArray;  
myAges =[10, 18, 25];  
console.log("My age is: " +myAges[1]);  
// 
interface Person {  
  firstName: string;  
  lastName: string;  
  age: number;  
 
}  
class Employee implements Person {  
  firstName: string;  
  lastName: string;  
  age:number;  
  FullName() {  
      return this.firstName + ' ' + this.lastName;  
  }  
  GetAge() {  
      return this.age;  
  }  
  constructor(firstN: string, lastN: string, getAge: number) {  
      this.firstName = firstN;  
      this.lastName = lastN;  
      this.age = getAge;  
  }  
}  
let myEmployee = new Employee('kapardee', 'sharma', 25);  
let fullName = myEmployee.FullName();  
let Age = myEmployee.GetAge();  
console.log("Name of Person: " +fullName + '\nAge: ' + Age);  
// 
module Sum {   
  export function add(a: any, b: any) {    
     console.log("Sum: " +(a+b));   
  }   
}  
// 
function identity(arg: string | number) {  
  return arg;  
}  
var output1 = identity("myString");  
var output2 = identity(100);  
console.log(output1);  
console.log(output2);  
// 
interface Lengthwise {  
  length: number;  
}  
function loggingIdentity<T extends Lengthwise>(arg: T): T {  
  console.log("Length: " +arg.length);   
  return arg;  
}  
loggingIdentity({length: 10, value: 9});  
// 
let date: Date = new Date();  
console.log("Date = " + date); 
// 
let dated: Date = new Date(2017, 4, 4, 17, 23, 42, 11);  
dated.setDate(13);  
dated.setMonth(13);  
dated.setFullYear(2021);  
dated.setHours(13);  
dated.setMinutes(13);  
dated.setSeconds(13);  
console.log("Year = " + dated.getFullYear());  
console.log("Date = " + dated.getDate());  
console.log("Month = " + dated.getMonth());  
console.log("Day = " + dated.getDay());  
console.log("Hours = " + dated.getHours());  
console.log("Minutes = " + dated.getMinutes());  
console.log("Seconds = " + dated.getSeconds()); 
// 





